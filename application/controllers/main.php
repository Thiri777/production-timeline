<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	
	public function __construct(){
		parent::__construct();
		$this->load->model('main_model');
	}

	public function index()
	{
		
		if(isset($this->session->userdata['logged_in'])){
			$this->load->view('template/header');
			$this->load->view('timeline');
			$this->load->view('template/footer');
		}
		else{
			$this->load->view('template/header');
	    	$this->load->view('login_view');
	    	$this->load->view('template/footer');
		}
	}

	public function getMainContent(){
		if(isset($this->session->userdata['logged_in'])){
			if(isset($_GET)){
				//Load Models
				$uData = $this->session->userdata('logged_in');
				$uType = $uData['type'];
				$subject = $this->main_model->getSubject($uType);
				$data['subject'] = $subject;

				$weekinfo = $this->main_model->getWeekInfo();
				$data['weekinfo'] = $weekinfo;

				$res = ''; 

				foreach ($subject as $row) {
					$subjectID = $row->id;
					$subject = $row->name;
				
					$popupBox = ''; $formData = '';

					$res .= '<div class="col-md-12">
								
								<div class="col-md-1">'.$subject.'</div>
								<div class="col-md-11">';
									
									foreach ($weekinfo as $week) {

										$weekID = $week->week_no;
										

										$status = 0; $deploy = ''; $weekTitle = ''; $filePath = ''; $bgImage = '';$refID=0; $ID = 0;
										$is_reference = 0;

										$weekDetail = $this->getWeekDetail($subjectID,$weekID);
										if($weekDetail && sizeof($weekDetail) > 0){
											$ID = $weekDetail[0]->ID;
											$weekTitle = $weekDetail[0]->week_title;
											$status = $weekDetail[0]->status;
											$filePath = $weekDetail[0]->bfile_path;
											$is_reference = $weekDetail[0]->is_reference;
											$refID = $weekDetail[0]->ref_id;
										}
										

										if($status == 1){	//data present
											$color = '#c0e05a';
											$heading = 'WEEK '.$weekID;
											$formStr = $this->getFormData($subjectID,$weekID,$status,$weekTitle,$filePath,$is_reference,$refID,$ID);

											$btnDownload = "btnDownload_".$subjectID."_".$weekID;
											$js = 'onclick="downloadFile(\''.$subjectID.'\',\''.$weekID.'\');"';

											$downloadStr = '<p>'.$filePath.'</p>';
											$downloadStr .= '<p>'.form_button(array('name' => 'btnDownload', 'id' => $btnDownload , 'type' => 'button', 'content' => 'Download', 'class' => 'btn btn-primary btn-xs'),'',$js).'</p>';
											$lock  = form_button(array('name' => 'btnLock_'.$subjectID.'_'.$weekID , 'id' => 'btnLock_'.$subjectID.'_'.$weekID , 'type' => 'button', 'content' => 'Lock this for production', 'class' => 'btn btn-primary btn-xs'),'', $js='onclick="changeStatus(\''.$subjectID.'\',\''.$weekID.'\',\'2\');"');
											$formData = '<p class="heading">'.$heading.'</p><p class="heading">Ref ID <mark>'.$ID.'</mark></p>'.$formStr.'<div style="text-align:center;">'.$downloadStr.$lock.'</div>';

										}
										elseif($status == 2){	// Lock
											$color = '#f07241';
											$formData = $this->getData($subjectID,$weekID,$status,$weekTitle,$filePath,$ID);

										}
										elseif($status == 3){
											$color = '#c04848';
											$formData = $this->getData($subjectID,$weekID,$status,$weekTitle,$filePath,$ID);
										}
										elseif($status == 4){
											$color = '#601848';
											$formData = $this->getData($subjectID,$weekID,$status,$weekTitle,$filePath,$ID);
										}
										elseif($status == 5){
											$color = '#300030';
											$formData = $this->getData($subjectID,$weekID,$status,$weekTitle,$filePath,$ID);
										}
										elseif($status == 6){
											$color = '#000000';
											$formData = $this->getData($subjectID,$weekID,$status,$weekTitle,$filePath,$ID);
											//$deploy = '<i class="glyphicon glyphicon-star glyphicon-white"></i>';
											$bgImage = ' url(./assets/star-16.gif) center center no-repeat' ;

										}
										elseif($status == 0)
										{
											$color = '#dddcdc';
											$heading = 'WEEK '.$weekID;
											$formData = '<p class="heading">'.$heading.'</p>'.$this->getFormData($subjectID,$weekID,$status,$weekTitle,$filePath,$is_reference,$refID,$ID);

										}
										else{
											$color = '#00BFFF';
											$heading = 'WEEK '.$weekID;
											$formData = '<p class="heading">'.$heading.'</p>'.$this->getFormData($subjectID,$weekID,$status,$weekTitle,$filePath,$is_reference,$refID,$ID);
											

										}

										$res .= '<div id="slot_'.$subjectID.'_'.$weekID.'" style="background : '.$color. $bgImage. ' " onclick="showData(\''.$ID.'\',\''.$weekID.'\','.$subjectID.');" data-toggle="tooltip" class="timeslot" title="WEEK '.$weekID.'" >'.$deploy.'</div>';
									

										$popupBox .= '<div class="col-md-12">
														<div class="col-md-1"></div>
														
														<div class="col-md-11 popupBox" id="dataBox_'.$subjectID.'_'.$weekID.'" style="border:1px solid black;display:none;" ">'.
															
															$formData
														.'</div>
														
													</div>';
									}
								


					$res .= '</div>'.$popupBox.'</div>';

				
				}

				echo $res;

			}
		}
		else{
			$this->load->view('template/header');
	    	$this->load->view('login_view');
	    	$this->load->view('template/footer');
		}
	}

	public function getFormData($subjectID,$weekID,$status,$weekTitle,$filePath,$is_reference,$refID,$ID)
	{

		$frmAttr = array('name' => 'frmAction_'.$subjectID.'_'.$weekID, 'id' => 'frmAction_'.$subjectID.'_'.$weekID , 'class' => 'form-horizontal', 'role' => 'form');
		$lblArr = array('class' => 'control-label col-md-2');
		$frmButton = array('name' => 'btnSubmit', 'id' => 'btnSubmit', 'type' => 'submit', 'content' => 'Save and Upload', 'class' => 'btn btn-primary btn-xs');
		
		$hidden = array('ID' => $ID,
						'subjectID' => $subjectID, 
						'weekNo' => $weekID,
						'status' => $status,
						'file' => $filePath
						);
		$txtTitle = array('name' => 'txtTitle', 'id' => 'txtTitle', 'value' => $weekTitle, 'required' => 'required','class' => 'form-control');
		
		if($status == 0)
			$fileInput = form_upload(array('name' =>'myfile','id' =>'myfile', 'required' => 'required'));
		elseif($status > 0)
			$fileInput = form_upload(array('name' =>'myfile','id' =>'myfile'));
		
		$formStr = form_open_multipart('main/formAction', $frmAttr,$hidden);
		
			if($status > 0 && $status < 7){

				$formStr .= '<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'.form_label("Title","title",$lblArr).'<div class="col-md-10">'.form_input($txtTitle).'</div></div>';
				$formStr .= '<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'.form_label("File","file",$lblArr).'<div class="col-md-10">'.$fileInput.'</div></div>';
				$formStr .= '<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm"><div class="col-md-offset-2 col-md-2">'.form_button($frmButton).'</div></div>';
			
			}
			else
			{

				if($is_reference == 1){
					$refChecked = 'checked';$norChecked = '';
				}
				else{
					$norChecked = 'checked';$refChecked = '';
				}

				$formStr .= '<div style="margin-bottom:10px;" id="optDiv_'.$subjectID.'_'.$weekID.'" name="optDiv_'.$subjectID.'_'.$weekID.'"><label class="radio-inline">
							<input value = "1" type="radio" name="optradio_'.$subjectID.'_'.$weekID.'" '.$refChecked.'>Reference
							</label>
							<label class="radio-inline">
							<input value="0" type="radio" name="optradio_'.$subjectID.'_'.$weekID.'" '.$norChecked.'>Normal
						</label></div>';

				if($is_reference == 1){
											
					/*$formStr .= '<div id="divRef_'.$subjectID.'_'.$weekID.'" name="divRef_'.$subjectID.'_'.$weekID.'">
					<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'.form_label("Title","title",array("class" => "control-label col-md-4")).'<div class="col-md-8">'.form_input($txtTitle).'</div></div>
					<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'
					.form_label("Ref ID","ref_weekID", array("class" => "control-label col-md-4")).'<div class="col-md-8">'
					.form_input(array("class" => "form-control", "id" => "txtref_weekID_".$subjectID."_".$weekID, "name" => "txtref_weekID_".$subjectID."_".$weekID, "required" => "required", "value" => "$refID")).'</div></div>
					<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'.form_label("","",array("class" => "control-label col-md-4", "style" => "text-align:left;")).'<div class="col-md-8">'
					.form_button(array("id" => "btnRef", "name" => "btnRef", "type" => "submit" , "content" => "Save", "class" => "btn btn-primary btn-xs")).'</div></div>
					</div>';*/
				}
				
			}
		$formStr .= form_close();

		return $formStr;
	}

	public function getDataByOpt(){
		if(isset($this->session->userdata['logged_in'])){
			if(isset($_POST['optValue'])){
				$optValue = $_POST['optValue'];
				$subjectID = $_POST['subID'];
				$weekID = $_POST['weekID'];
				$ID = $_POST['ID'];
				$weekTitle = '';
				$formStr = '';
				$refID = '';
				$refStr = '';

				if($optValue == 1){
					$numRows = $this->main_model->getAllWeek();
					
					if($numRows == 0){
						echo $optValue.'###<div id="divRef_'.$subjectID.'_'.$weekID.'" name="divRef_'.$subjectID.'_'.$weekID.'" class="errMsg"><p>No Reference Data Found!</p></div>';
					}
					else
					{
						$weekDetail = $this->getWeekDetailByID($ID,$subjectID,$weekID);
						if($weekDetail && sizeof($weekDetail) > 0){
							$ID = $weekDetail[0]->ID;
							$weekTitle = $weekDetail[0]->week_title;
							$status = $weekDetail[0]->status;
							$filePath = $weekDetail[0]->bfile_path;
							$is_reference = $weekDetail[0]->is_reference;
							$refID = $weekDetail[0]->ref_id;
						}

						if($refID != ''){
							$refSubject = '00'; $refWeekNo = '00'; 

							$refDataRes = $this->main_model->getRefDataByID($refID);
							if($refDataRes){
								foreach ($refDataRes as $index => $rowObj) {
									$refSubject = $rowObj->name;
									$refWeekNo = $rowObj->week_no;
								}

								$refStr = '<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'.form_label("","title",array("class" => "control-label col-md-4")).'<div class="col-md-8"><span style="font-size:12px;background-color:#FFFF00;font-weight:700;">'.$refSubject.' -> week '.$refWeekNo.'</span></div></div>';
							}
						}
						

						$txtTitle = array('name' => 'txtTitle', 'id' => 'txtTitle', 'value' => $weekTitle, 'required' => 'required','class' => 'form-control');

						$refData = '<div id="divRef_'.$subjectID.'_'.$weekID.'" name="divRef_'.$subjectID.'_'.$weekID.'">
						<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'.form_label("Title","title",array("class" => "control-label col-md-4")).'<div class="col-md-8">'.form_input($txtTitle).'</div></div>
						<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'
						.form_label("Ref ID","ref_weekID", array("class" => "control-label col-md-4")).'<div class="col-md-8">'
						.form_input(array("class" => "form-control", "id" => "txtref_weekID_".$subjectID."_".$weekID, "name" => "txtref_weekID_".$subjectID."_".$weekID, "required" => "required","value" => "$refID")).'</div></div>'
						.$refStr.'<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'.form_label("","",array("class" => "control-label col-md-4", "style" => "text-align:left;")).'<div class="col-md-8">'.form_button(array("id" => "btnRef", "name" => "btnRef", "type" => "submit" , "content" => "Save", "class" => "btn btn-primary btn-xs")).'</div></div>
						
						</div>';
						echo $optValue.'###'.$refData;
					}
				}
				else
				{
					$lblArr = array('class' => 'control-label col-md-2');
					$txtTitle = array('name' => 'txtTitle', 'id' => 'txtTitle', 'value' => $weekTitle, 'required' => 'required','class' => 'form-control');
					$fileInput = form_upload(array('name' =>'myfile','id' =>'myfile', 'required' => 'required'));
					$frmButton = array('name' => 'btnSubmit', 'id' => 'btnSubmit', 'type' => 'submit', 'content' => 'Save and Upload', 'class' => 'btn btn-primary btn-xs');

					$formStr .= '<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'.form_label("Title","title",$lblArr).'<div class="col-md-10">'.form_input($txtTitle).'</div></div>';
					$formStr .= '<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'.form_label("File","file",$lblArr).'<div class="col-md-10">'.$fileInput.'</div></div>';
					$formStr .= '<div name="frmgp_'.$subjectID.'_'.$weekID.'" class="form-group form-group-sm">'.form_label("","",$lblArr).'<div class="col-md-10">'.form_button($frmButton).'</div></div>';
				
					echo $optValue.'###'.$formStr;

				}

				
			}
		}
		else{
			$this->load->view('template/header');
	    	$this->load->view('login_view');
	    	$this->load->view('template/footer');
		}
	}

	public function getData($subjectID,$weekID,$status,$weekTitle,$filePath,$ID){

		$btnDownload = "btnDownload_".$subjectID."_".$weekID;
		$js = 'onclick="downloadFile(\''.$subjectID.'\',\''.$weekID.'\');"';

		$downloadStr = '<p>'.$filePath.'</p>';
		$downloadStr .= '<p>'.form_button(array('name' => 'btnDownload', 'id' => $btnDownload , 'type' => 'button', 'content' => 'Download', 'class' => 'btn btn-primary btn-xs'),'',$js).'</p>';

		$lock  = form_button(array('name' => 'btnLock_'.$subjectID.'_'.$weekID , 'id' => 'btnLock_'.$subjectID.'_'.$weekID , 'type' => 'button', 'content' => 'Lock this for production', 'class' => 'btn btn-primary btn-xs'),'', $js='onclick="changeStatus(\''.$subjectID.'\',\''.$weekID.'\',\'2\');"');
		
		

		
		if($status == 1)	// data exist
		{
			$heading = 'WEEK '.$weekID;
			$rtnStr = $formStr.'<div style="text-align:center;">'.$downloadStr.$lock.'</div>';
		}
		elseif($status == 2 OR $status == 3 OR $status == 4 OR $status == 5 OR $status == 6)	// lock
		{
			$heading = '<p class="heading">WEEK '.$weekID.'</p><p class="heading">'.$weekTitle.'</p>';

			switch ($status) {
				case 2:
					$statusStr = 'Lock';
					break;
				case 3:
					$statusStr = 'Production Started';
					
					break;
				case 4:
					$statusStr = 'Draft Done';
					
					break;
				case 5:
					$statusStr = 'Approved';
					break;
				case 6:
					$statusStr = 'Ready For Deployment';
					break;
				
			}
			
			$rtnStr = '<div style="text-align:center;">'.$downloadStr;
			$rtnStr .= '<p>Current Status "'.$statusStr.'" </p>';
			$rtnStr .= '<p><header>Change Status</header></p>';
			if($status == 3)
				$rtnStr .= '<p>'.form_button(array('name' => 'btnPro', 'id' => 'btnPro', 'type' => 'button', 'content' => 'Production Started', 'class' => 'btn btn-primary btn-xs', 'disabled' => 'disabled'),'', $js='onclick="changeStatus(\''.$subjectID.'\',\''.$weekID.'\',\'3\');"').'</p>';
			else
				$rtnStr .= '<p>'.form_button(array('name' => 'btnPro', 'id' => 'btnPro', 'type' => 'button', 'content' => 'Production Started', 'class' => 'btn btn-primary btn-xs' ),'', $js='onclick="changeStatus(\''.$subjectID.'\',\''.$weekID.'\',\'3\');"').'</p>';
			
			if($status == 4)
				$rtnStr .= '<p>'.form_button(array('name' => 'btnDraft', 'id' => 'btnDraft', 'type' => 'button', 'content' => 'Draft Done', 'class' => 'btn btn-primary btn-xs', 'disabled' => 'disabled' ),'', $js='onclick="changeStatus(\''.$subjectID.'\',\''.$weekID.'\',\'4\');"').'</p>';
			else
				$rtnStr .= '<p>'.form_button(array('name' => 'btnDraft', 'id' => 'btnDraft', 'type' => 'button', 'content' => 'Draft Done', 'class' => 'btn btn-primary btn-xs' ),'', $js='onclick="changeStatus(\''.$subjectID.'\',\''.$weekID.'\',\'4\');"').'</p>';
			

			if($status == 5)
				$rtnStr .= '<p>'.form_button(array('name' => 'btnApproved', 'id' => 'btnApproved', 'type' => 'button', 'content' => 'Approved', 'class' => 'btn btn-primary btn-xs' , 'disabled' => 'disabled'),'', $js='onclick="changeStatus(\''.$subjectID.'\',\''.$weekID.'\',\'5\');"').'</p>';
			else
				$rtnStr .= '<p>'.form_button(array('name' => 'btnApproved', 'id' => 'btnApproved', 'type' => 'button', 'content' => 'Approved', 'class' => 'btn btn-primary btn-xs'),'', $js='onclick="changeStatus(\''.$subjectID.'\',\''.$weekID.'\',\'5\');"').'</p>';
			

			if($status == 6)
				$rtnStr .= '<p>'.form_button(array('name' => 'btnDeploy', 'id' => 'btnDeploy', 'type' => 'button', 'content' => 'Ready For Deployment', 'class' => 'btn btn-primary btn-xs' , 'disabled' => 'disabled'),'', $js='onclick="changeStatus(\''.$subjectID.'\',\''.$weekID.'\',\'6\');"').'</p>';
			else
				$rtnStr .= '<p>'.form_button(array('name' => 'btnDeploy', 'id' => 'btnDeploy', 'type' => 'button', 'content' => 'Ready For Deployment', 'class' => 'btn btn-primary btn-xs'),'', $js='onclick="changeStatus(\''.$subjectID.'\',\''.$weekID.'\',\'6\');"').'</p>';
			$rtnStr .= '</div>';
		}

		return $heading.'<p class="heading">Ref ID <mark>'.$ID.'</mark></p>'.$rtnStr;

	}

	public function getWeekDetail($subjectID,$weekID){

		//Load Models
		
		$wDetail = $this->main_model->getWeekDetail($subjectID,$weekID);
		return $wDetail;
	}

	public function getWeekDetailByID($ID,$subjectID,$weekID){
		$wDetail = $this->main_model->getWeekDetailByID($ID,$subjectID,$weekID);
		return $wDetail;
	}

	public function formAction(){
		if(isset($this->session->userdata['logged_in'])){
			if($_SERVER["REQUEST_METHOD"] == "POST"){
				
				$ID = $_POST['ID'];
				$subjectID = (isset($_POST['subjectID']))? $_POST['subjectID']:0;
				$weekNo = (isset($_POST['weekNo'])) ? $_POST['weekNo'] : '00';
				$optValue = (isset($_POST['optradio_'.$subjectID.'_'.$weekNo])) ? $_POST['optradio_'.$subjectID.'_'.$weekNo]: 0;
				$status = $_POST['status'];
				$title = $_POST['txtTitle'];
				$oldfile = $_POST['file'];
				$refID = (isset($_POST['txtref_weekID_'.$subjectID.'_'.$weekNo])) ? $_POST['txtref_weekID_'.$subjectID.'_'.$weekNo] : 'NULL';

				$weekRes = $this->getWeekDetailByID($ID,$subjectID,$weekNo);
				//print_r($weekRes);exit;
				$file = (!empty($_FILES["myfile"]["name"])) ? $_FILES["myfile"]["name"] : '';
				
				if($file != ''){
					$targetDir = "./assets/uploads/";
					if(!file_exists($targetDir)){
						mkdir($targetDir);
					}
					$random = md5(uniqid());
					$ext = end((explode(".", $file)));
					$targetFileName = substr($random, 0,11).'.'.$ext;

					$targetFile = $targetDir.$targetFileName;
					
					if (move_uploaded_file($_FILES["myfile"]["tmp_name"], $targetFile)) {
				           
				    } 
				    else {
				        echo "Sorry, there was an error uploading your file.";
				    }

				    $bookfile = $targetFileName;
				}
				else{
					$bookfile = $oldfile;
				}
				

			    if($status == 0 && sizeof($weekRes) == 0){	//Add New Data
			    	$status = ($optValue==1) ? 7 : 1;
			    	$uniqueID = substr(md5(uniqid(rand(), true)),0,5);
		        	$data = array('ID' => $uniqueID, 
		        					'week_no' => $weekNo,
		        					'week_title' => $title,
		        					'subject_id' => $subjectID,
		        					'bfile_path' => $bookfile,
		        					'status' => $status,
		        					'is_reference' => $optValue,
		        					'ref_id' => $refID
		        				);
		        	$res = $this->main_model->saveWeekDetail($data);
		        	if($res){
		        		redirect('main/','refresh');
		        	}
		        }
		        elseif(($status == 1 || $status == 7) && sizeof($weekRes) > 0)
		        {	//Update Existing Data
		        	//echo 'Status => '.$status.' and Option Value => '.$optValue;exit;
		        	if($status == 7 && $optValue == 0){
		        		$status = 1;

		        	}
		        	$data = array(
		      						'week_no' => $weekNo,
		        					'week_title' => $title,
		        					'subject_id' => $subjectID,
		        					'bfile_path' => $bookfile,
		        					'status' => $status,
		        					'is_reference' => $optValue,
		        					'ref_id' => $refID
		        				);

		        	$res = $this->main_model->updateWeekDetail($data,$ID,$subjectID,$weekNo);
		        	if($res){
		        		redirect('main/','refresh');
		        	}

		        }
				
			}
		}
		else{
			$this->load->view('template/header');
	    	$this->load->view('login_view');
	    	$this->load->view('template/footer');
		}

	}

	
	public function bookDownload($subID, $weekNo){

		if(isset($this->session->userdata['logged_in'])){
			$rtnRes = $this->getOutPutFileName($subID, $weekNo);
			$rtnArr = explode('#@#', $rtnRes);
			$outputFileName = $rtnArr[0];
			$file = $rtnArr[1];

			$file = './assets/uploads/'.$file;

			

	        if (file_exists($file)) {
	        	if(!is_writable($file)){
		            chmod($file, 0755);
		        }

	            header('Content-Description: File Transfer');
	            header('Content-Type: application/octet-stream');
	            header('Content-Disposition: attachment; filename='.basename($outputFileName));
	            header('Expires: 0');
	            header('Cache-Control: must-revalidate');
	            header('Pragma: public');
	            header('Content-Length: ' . filesize($file));

	            //readfile($file);
	            //$fp = fopen($file, "r");
	            //fpassthru($fp);
	            readfile($file);
	            exit;
	        }
	        else{
	            echo 'file_not_exist';exit;
	        }
	    }
	    else{
			$this->load->view('template/header');
	    	$this->load->view('login_view');
	    	$this->load->view('template/footer');
		}
	}


	public function getOutPutFileName($subID, $weekNo){
		date_default_timezone_set("Asia/Singapore");
		$res = $this->main_model->getWeekData($subID, $weekNo);
		$curDate = date('Ymdhi');

		if($res && sizeof($res) > 0){
			foreach ($res as $rIndex => $RowObj) {
				# code...
				$weekNo = $RowObj->week_no;
				$subID = $RowObj->subject_id;
				$weekTitle = $RowObj->week_title;
				$bfile = $RowObj->bfile_path;
				$subName = $RowObj->name;
			}


		}
		$ext = end((explode(".", $bfile)));
		$outPutFile = $subName.'-'.$weekTitle.'-'.$weekNo.'-'.$curDate.'.'.$ext;

		return $outPutFile.'#@#'.$bfile;
	}


	public function changeStatus(){
		if(isset($this->session->userdata['logged_in'])){
			if(isset($_POST['subID']) && isset($_POST['weekNo']) && isset($_POST['mode'])){
				if($this->main_model->changeStatus($_POST['subID'],$_POST['weekNo'],$_POST['mode']))
					echo 'success';
				else
					echo 'fail';
			}
		}
		else{
			$this->load->view('template/header');
	    	$this->load->view('login_view');
	    	$this->load->view('template/footer');
		}
		
	}


	public function check_reference(){
		if(!empty($_POST['Ref_ID'])){
			
			$refID = $_POST['Ref_ID'];
			$res = $this->main_model->check_reference($refID);

			if($res){
				echo 'true';
			}
			else{
				echo 'false';
			}
		}
		else{
			echo 'empty';
		}
	}












	
				
				
					
	















}