<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('login_model');
		$this->load->helper('cookie');
	}

	public function index()
	{
		
		$this->load->view('template/header');
		$this->load->view('login_view');
		$this->load->view('template/footer');
	}

	public function verify_login(){

		//This method will have the credentials validation
	   $this->load->library('form_validation');
	 
	   $this->form_validation->set_rules('txtname', 'Email', 'trim|required|valid_email| xss_clean');
	   $this->form_validation->set_rules('txtpassword', 'Password', 'trim|required|xss_clean|callback_check_database');

	   	/*$remember = $this->input->post('keeplogin');
		if($remember == 1)
		{
			$cookie = array(
					'name' => 'keeplogin',
					'value' => $this->input->post('keeplogin'),
					'expire' => 86400*30,
					'path' => '/',
					'secure' => true
					);
			$this->input->set_cookie($cookie);

			
			
		}
		*/

		
	   if($this->form_validation->run() == FALSE)
	   {
	     //Field validation failed.  User redirected to login page

	   		$this->load->view('template/header');
	    	$this->load->view('login_view');
	    	$this->load->view('template/footer');
	   }
	   else
	   {
	     //Go to private area
	     redirect('main', 'refresh');
	   }
	}

	public function forgotPassword(){

		$this->load->view('template/header');
    	$this->load->view('login_view', array("mode" => "forgot"));
    	$this->load->view('template/footer');
	}

	public function reset_password(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('txtfname','Name','required|trim|valid_email|xss_clean|callback_change');
		$this->form_validation->set_rules('txtnpassword','New Password','required|trim');
		$this->form_validation->set_rules('txtcpassword','Confirm Password','required|trim|matches[txtnpassword]');

		if($this->form_validation->run()!= true)
		{
			$this->load->view('template/header');
	    	$this->load->view('login_view', array("mode" => "forgot"));
	    	$this->load->view('template/footer');

		}
		else{
			$this->saveNewPassword();
		}
	}

	function saveNewPassword(){
		$username = $this->input->post('txtfname');
		$txtnpassword = $this->input->post('txtnpassword');
		$txtcpassword = $this->input->post('txtcpassword');

		$data = array('username' => $username,'txtnpassword' => md5($txtnpassword));

		$result = $this->login_model->saveNewPassword($data);

		if($result){
			$data['success'] = "Password Reset Successful! Please login again.";
			$this->load->view('template/header');
	    	$this->load->view('login_view',$data);
	    	$this->load->view('template/footer');
		}
		else{
			$data['mode'] = "forgot";
			$data['reset_fail'] = "Password Reset Fail!";
			$this->load->view('template/header');
	    	$this->load->view('login_view', $data);
	    	$this->load->view('template/footer');
		}

	}

	function change(){
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('txtfname');

		//query the database
		$result = $this->login_model->checkUser($username);

		if($result)
		{
			/*$sess_array = array();
			foreach($result as $rowObj)
			{
				$sess_array = array(
					'id' => $rowObj->id,
					'username' => $rowObj->username
				);
				$this->session->set_userdata('logged_in', $sess_array);
			}*/
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('change', 'Your Account does not already exist!');
			return false;
		}
	}

	function check_database($password)
	{
		//Field validation succeeded.  Validate against database
		$username = $this->input->post('txtname');

		//query the database
		$result = $this->login_model->login($username, $password);

		if($result)
		{
			$sess_array = array();
			foreach($result as $rowObj)
			{
				$sess_array = array(
					'id' => $rowObj->id,
					'username' => $rowObj->username,
					'type' => $rowObj->user_type
				);
				$this->session->set_userdata('logged_in', $sess_array);
			}


			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('check_database', 'Invalid username or password');
			return false;
		}
	}

	public function logout() {

		// Removing session data
		$sess_array = array(
			'id' => 0,
			'username' => ''
		);

		$this->session->unset_userdata('logged_in', $sess_array);
		//$data['message_display'] = 'Successfully Logout';
		$this->load->view('template/header');
    	$this->load->view('login_view');
    	$this->load->view('template/footer');
	}

}


?>