
<style>
	.timeslot{
		background-color: #dddcdc;
		color:white;
		border-right: 1px solid #EEE;
		height: 20px;
		padding:0px;
		line-height: 30px;
		float:left;
		width: 20px;
	}

	.arrow-up {
		width: 0; 
		height: 0; 
		border-left: 5px solid transparent;
		border-right: 5px solid transparent;
		border-bottom: 5px solid black;
	}

	.popupBox{
		width:auto;
		height:auto;
		margin-top:5px;
		margin-bottom:5px;
		padding-top:5px;
		padding-bottom:5px;
	}

	.footer{
	    position:fixed;
	    bottom:0;
	    width:100%;
	    height:20px;
	}

	.container {
	   width: 1300px !important;
	 }

	.errMsg{
		color: red;
	    font-size: smaller;
	    padding-top: 5px;
	}

	.heading{
		text-align: center;
	    font-size: 12px;
	    font-weight: 700;
	    text-transform: uppercase;
	}

	mark{
		background-color:#FFFF00;
	}

	#divLogout{
		text-align: center;
	    margin-top: 10px;
	    font-size: 20px;
	}

	
</style>
<div class="row"><div class="col-md-12">
	<div id="divLogout"><?php echo anchor('login/logout',"Log out"); ?></div></div>
</div><br/>
<div class="row"><div class="col-md-12"><br/></div></div>
<div class="row">
	<!-- Main Content -->
	<div id="legendDiv" style="text-align:center;"><div class="col-md-12"><img src="<?php echo base_url();?>assets/images/productionLegend.png" /></div></div>	
	<div id="main_content"></div>
</div>

<!-- Jquery Start Here -->
<script>
	
	$(function(){
		
		$.ajax({

			type : "GET",
			url : "<?php echo base_url();?>main/getMainContent",

		}).done(function(data) {

			if(data.match('frmLogin')){
				location.href = "<?php echo base_url();?>login/";
			}
			
			$('#main_content').html(data);

			$('[data-toggle="tooltip"]').tooltip({
				trigger: "hover",
				placement: "top",
				
			}); 



			
		});


		

	});

	//For Dynamically Created html elements
	

	function showHoverData(weekID,subID){
		ID = 'slot_'+subID+'_'+weekID;
		$('#'+ID).tooltip({
			placement: "top",
			title : "WEEK "+weekID
		}).tooltip('show');
	}

	function showData(ref_ID, week_ID,sub_ID){
		
		ID = ref_ID;
		weekID = week_ID;
		subID = sub_ID;
		
		if(ID == 0)
			req = true;
		else
			req = false;

		$('.popupBox').hide();
		boxID = 'dataBox_'+subID+'_'+weekID;
		$('#'+boxID).show();
		
		frmID = 'frmAction_'+subID+'_'+weekID;
		refID = 'txtref_weekID_'+subID+'_'+weekID;

		divRefID = 'divRef_'+subID+'_'+weekID;
		optRadioEle = $("[name=optradio_"+subID+"_"+weekID+"]");
		//alert("Option Element => "+optRadioEle.length);
		//return false;
		if(optRadioEle.length > 0){
			optElementChk = $('input[name="optradio_'+subID+'_'+weekID+'"]:checked');

			if(optElementChk){
				//alert("Enable Option ");
				optAction(optElementChk,ID,subID,weekID);
			}
		}
		
		

		jQuery.validator.setDefaults({
		  debug: false,
		  success: "valid"
		});

		validateForm(frmID,req);
		

		$('input[name="optradio_'+subID+'_'+weekID+'"]').click(function(){
			//alert('click');
			optAction(this,ID,subID,weekID);

		});
	
	}

	function validateForm(frmID,req){
		$( "#"+frmID ).validate({
			errorElement: 'div',
			errorClass: 'errMsg',
			/*errorPlacement: function(error, element) {
			 	if (element.attr("name") == "myFieldName") {

			        error.insertBefore( $("#errMsg") );
				    $("#errMsg").html( error );  

				} 
				else 
				{
			        error.insertAfter(element);

			    }
			 	
			 },
			*/
			rules: {
				myfile: {
				  required: req,
				  //accept: "application/pdf,application/msword,application/zip"
				  extension : "doc|docx|pdf|zip|ppt|pptx"
				  
				}
				/*txtref_weekID : {
					required: true,
					remote :{
						type : "POST",
						url : "<?php echo base_url();?>main/check_reference",
						data : {
							Ref_ID : function(){
								return $('#'+refID).val();
							}
						}
					}
					
				}*/
			},

			//Custom Message For Rules
			messages: {
				myfile : {
					extension : "Please Enter Valid File Type."
				}
			    
			}

		});
	}

	function optAction(element,ID,subID,weekID){
		//alert("element => "+element);
		optValue = $(element).val();
		frmID = 'frmAction_'+subID+'_'+weekID;

		if(optValue != undefined){
			$.ajax({
				data: {
					"optValue" : optValue,
					"subID" : subID,
					"weekID" : weekID,
					"ID" : ID
				},
				type : "POST",
				url : "<?php echo base_url();?>main/getDataByOpt",

			}).done(function(data) 
			{
				
				//login = $(source).find('#frmLogin').html();
				
				if(data.match('frmLogin')){
					location.href = "<?php echo base_url();?>login/";
				}

				dataArr = data.split('###');
				opt = dataArr[0];dataRef='';dataForm='';

				if(opt == 1){
					dataRef = dataArr[1];req = false;
				}
				else{
					dataForm = dataArr[1];req = true;
				}


				if(dataRef){
					//alert("form length => "+$("[name=frmgp_"+subID+"_"+weekID+"]").length);
					if($("[name=frmgp_"+subID+"_"+weekID+"]").length > 0){
						$("[name=frmgp_"+subID+"_"+weekID+"]").remove();
					}
					//alert("divRef length => "+$('#'+divRefID).length);
					if($('#'+divRefID).length > 0){
						$('#'+divRefID).remove();
					}

					$(dataRef).insertAfter('#optDiv_'+subID+'_'+weekID);

					//Custom Validation for RefID
					if (document.getElementById(refID)) {
						//alert('validate ref id');
						//VALIDATE Reference ID
						$('#'+refID).rules("add",
						{
							required : true,
							
							"remote" :
									{
									  url: '<?php echo base_url();?>main/check_reference',
									  type: "post",
									  data:
									  {
									      Ref_ID: function()
									      {
									          return $('#'+refID).val();
									      }
									  }
									},
							messages: 
							{
							    remote : "Reference ID is not existed."
							}
						});
					}
					//else{alert('ref id undefined');}

				}
				else ///Form with file upload
				{
					//alert("divRef length => "+$('#'+divRefID).length);
					if($('#'+divRefID).length > 0){
						$('#'+divRefID).remove();
					}

					//alert("form length => "+$("[name=frmgp_"+subID+"_"+weekID+"]").length);
					if($("[name=frmgp_"+subID+"_"+weekID+"]").length > 0){
						$("[name=frmgp_"+subID+"_"+weekID+"]").remove();
					}
					$(dataForm).insertAfter('#optDiv_'+subID+'_'+weekID);
				}

				validateForm(frmID,req);
				
			});
		}
			
	}

	function getContent(){
		return '<div>Content</div>';
	}

	function downloadFile(subID, weekNo)
	{
		window.location.href = "<?php echo base_url().'main/bookDownload/'?>" + subID + "/"+ weekNo;
	}

	function changeStatus(subID, weekNo,mode){
		//alert(weekNo);return false;
		$.ajax({

			type : "POST",
			url : "<?php echo base_url();?>main/changeStatus",
			data : {"subID" : subID,"weekNo":weekNo,"mode" : mode}

		}).done(function(data) { 
			//alert(data);
			if(data.match('frmLogin')){
				location.href = "<?php echo base_url();?>login/";
			}

			if(data == 'success')
				location.reload();

		});
	}


</script>
