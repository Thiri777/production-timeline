<style>
	#container {
		position: fixed;
		width: 300px;
		height: auto;
		top: 50%;
		left: 50%;
		margin-top: -140px;
		margin-left: -170px;
		background: #fff;
		border-radius: 3px;
		border: 1px solid #ccc;
		box-shadow: 0 1px 2px rgba(0, 0, 0, .1);
		-webkit-animation-name: bounceIn;
		-webkit-animation-fill-mode: both;
		-webkit-animation-duration: 1s;
		-webkit-animation-iteration-count: 1;
		-webkit-animation-timing-function: linear;
		-moz-animation-name: bounceIn;
		-moz-animation-fill-mode: both;
		-moz-animation-duration: 1s;
		-moz-animation-iteration-count: 1;
		-moz-animation-timing-function: linear;
		animation-name: bounceIn;
		animation-fill-mode: both;
		animation-duration: 1s;
		animation-iteration-count: 1;
		animation-timing-function: linear;
	}

	.alert-success, .alert-danger {
		font-size : 12px;
	}

	.alert {
		padding: 5px !important;
	}

	.forgot{
		margin : 10px 0px 0px 0px;
		font-size:12px;
	}


</style>
<div class="row">
	
	<div id="container">
		<div class="col-md-12">
			<!-- Begin Page Content -->
			<?php
				
				
				if(!isset($mode)){

					echo form_open('login/verify_login',array("id" => "frmLogin", "name" => "frmLogin", "class" => "form-horizontal"));
						echo '<fieldset><legend>Please Login</legend>';	
						
						if(isset($success)){
							echo '<div class="alert alert-success">
									<strong>'.$success.'</strong>
								</div>';
						}
						

						if(validation_errors()){
							echo '<div class="alert alert-danger">
									<strong>';
									echo validation_errors();
									echo '</strong>
								</div>';
						}
							
						
						echo '<div class="form-group">';
							//echo form_label("Email : ", "textinput", array("class" => "col-md-4 control-label"));
							echo '<div class="col-md-12">';
								echo form_input(array("name" => "txtname", "id" => "txtname", "type" => "email", "class" => "form-control input-md" ,"required" => "required" , "placeholder" => "someone@email.com"));
								echo '<p class="forgot">'.anchor('login/forgotPassword', 'Forgot your password?').'</p>';
							echo '</div>';
						echo '</div>';

						echo '<div class="form-group">';
							//echo form_label("Password : ", "password", array("class" => "col-md-4 control-label"));
							echo '<div class="col-md-12">';
								echo form_password(array("placeholder" => "Password" , "name" => "txtpassword", "class" => "form-control input-md" , "id" => "txtpassword" ,"required" => "required"));
								//echo '<p class="forgot">'.form_checkbox(array("id" => "keeplogin", "name" => "keeplogin","value" => "0")). 'Keep me logged in';
								echo '</p>';
							echo '</div>';
						echo '</div>';
						
						/*echo '<div class="form-group">';
							echo form_label("","checkboxes", array("class" => "col-md-4 control-label"));
							echo '<div class="col-md-12">';
								echo form_checkbox(array("id" => "keeplogin", "name" => "keeplogin","value" => "0")). 'Keep me logged in';
							echo '</div>';
						echo '</div>';
						*/

						echo '<div class="form-group">';
							//echo form_label("", "singlebutton",array("class" => "col-md-4 control-label"));
							echo '<div class="col-md-12">';
							echo form_submit(array("name" => "btnLogin", "id" => "btnLogin", "class" => "btn btn-primary", "value" => "Login"));
							echo '</div>';
						echo '</div>';

						echo '</fieldset>';
					echo form_close();
				}
				else{
					echo form_open('login/reset_password', array("id"=>"frmForgot", "name" => "frmForgot", "class" => "form-horizontal"));
						echo '<fieldset><legend>Password Reset</legend>';
						if(validation_errors() || isset($reset_fail)){
							$errMsg = (isset($reset_fail)) ? $reset_fail: validation_errors();
							echo '<div class="alert alert-danger">
									<strong>';
									echo $errMsg;
									echo '</strong>
								</div>';
						}
							echo '<div class="form-group">';
								//echo form_label("Email : ", "email", array("class" => "control-label col-md-5"));
								echo '<div class="col-md-12">';
									echo form_input(array("placeholder" => "someone@email.com" , "type" => "email", "name" => "txtfname", "id" => "txtfname", "class" => "form-control","required" => "required"));
								echo '</div>';
							echo '</div>';

							echo '<div class="form-group">';
								//echo form_label("New Password : ", "password", array("class" => "control-label col-md-5"));
								echo '<div class="col-md-12">';
									echo form_password(array("placeholder" => "Password" , "name" => "txtnpassword", "id" => "txtnpassword" ,"required" => "required", "class" => "form-control"));
								echo '</div>';
							echo '</div>';

							echo '<div class="form-group">';
								//echo form_label("Confirm Password : ", "password", array("class" => "control-label col-md-5"));
								echo '<div class="col-md-12">';
									echo form_password(array("placeholder" => "Confirm Password" , "name" => "txtcpassword", "id" => "txtcpassword" ,"required" => "required", "class" => "form-control"));
								echo '</div>';
							echo '</div>';
						
							echo '<div class="form-group">';
								//echo form_label("", "singlebutton",array("class" => "control-label col-md-5"));
								echo '<div class="col-md-12">';
									echo form_submit(array("name" => "btnPassReset", "id" => "btnPassReset", "class" => "btn btn-primary", "value" => "Password Reset"));
								echo '</div>';
							echo '</div>';
						
						echo '</fieldset>';
					echo form_close();
				}

				
				
			?>
			
			<!-- End Page Content -->
			<!--<form class="form-horizontal">
				<fieldset>

				<!-- Form Name -->
					<!--<legend>Please Login</legend>
					<!-- Alert 
					<div class="alert alert-success">
						<strong>Success!</strong> Indicates a successful or positive action.
					</div>
					<div class="alert alert-danger">
						<strong>Danger!</strong> Indicates a dangerous or potentially negative action.
					</div>-->
					<!-- Text input-->
					<!--<div class="form-group">
						<label class="col-md-4 control-label" for="textinput">Text Input</label>  
						<div class="col-md-8">
						<input id="textinput" name="textinput" type="text" placeholder="placeholder" class="form-control input-md">
						</div>
					</div>

					<!-- Password input-->
					<!--<div class="form-group">
						<label class="col-md-4 control-label" for="passwordinput">Password Input</label>
						<div class="col-md-8">
						<input id="passwordinput" name="passwordinput" type="password" placeholder="placeholder" class="form-control">

						</div>
					</div>

					<!-- Button -->
					<!--<div class="form-group">
						<label class="col-md-4 control-label" for="singlebutton"></label>
						<div class="col-md-8">
						<button id="singlebutton" name="singlebutton" class="btn btn-primary">Button</button>
						</div>
					</div>

					<!-- Multiple Checkboxes (inline) -->
					<!--<div class="form-group">
						<label class="col-md-4 control-label" for="checkboxes"></label>
						<div class="col-md-8">
							<label class="checkbox-inline" for="checkboxes-0">
							  <input type="checkbox" name="checkboxes" id="checkboxes-0" value="1">
							  1
							</label>
						
						</div>
					</div>

				</fieldset>
			</form>	-->
		</div>
	</div>
</div>
<script>
	
	$(function(){
		$('#keeplogin').click(function(){
			if($(this).val() == '0'){$(this).val(1);}else{$(this).val(0);}
		})
		//alert('ready');
	});
</script>