<?php
class Main_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getSubject($type)
    {
        if($type==1)
            $this->db->like('name', 'English');
        elseif($type==2)
            $this->db->like('name', 'Mathematics'); 
        elseif($type==3)
            $this->db->like('name', 'Science');
        elseif($type==4)
            $this->db->like('name', 'Chinese');

    	$query = $this->db->get('tbl_subject');
        //echo $query->num_rows();
    	return $query->result();
    }

    function getWeekInfo(){
    	$query = $this->db->get('tbl_week');
    	return $query->result();
    }

    function getAllWeek(){
        $this->db->order_by("week_no", "asc"); 
        $query = $this->db->get_where('tbl_week_detail', array('status <>' => 0));
        //echo $this->db->last_query();exit;
        return $query->num_rows();
    }

    function getWeekDetailByID($ID,$subjectID,$weekID){
        $this->db->order_by("week_no", "asc"); 
        $query = $this->db->get_where('tbl_week_detail', array('ID' => $ID, 'subject_id' => $subjectID,'week_no' => $weekID));
        return $query->result();
    }

    function getWeekDetail($subjectID,$weekID){
    	$this->db->order_by("week_no", "asc"); 
    	$query = $this->db->get_where('tbl_week_detail', array('subject_id' => $subjectID,'week_no' => $weekID));
    	return $query->result();
    }

    function getWeekData($subjectID,$weekID){
        $qry = 'SELECT wd.week_no,wd.week_title,wd.subject_id,wd.bfile_path,s.name FROM tbl_week_detail wd 
                left join tbl_subject s on (s.id = wd.subject_id)
                where wd.subject_id=? and wd.week_no=? ';

        $query = $this->db->query($qry, array($subjectID,$weekID));
        if($query)
            return $query->result();
    }

    function saveWeekDetail($data){
        if($this->db->insert('tbl_week_detail', $data)) return true;
        else return false; 
    }

    function updateWeekDetail($data,$ID,$subjectID,$weekNo){
        $this->db->where(array('ID' => $ID, 'subject_id' => $subjectID, 'week_no' => $weekNo));
        $res = $this->db->update('tbl_week_detail', $data); 
        if($res) return true;
        else return false;
    }

    function changeStatus($subID, $weekNo, $mode){
        $qry = 'UPDATE tbl_week_detail SET status=? WHERE subject_id=? and week_no=?';
        $res = $this->db->query($qry, array($mode,$subID,$weekNo));
        
        if($res)
            return true;
        else
            return false;
    }

    function check_reference($refID){
        $qry = 'SELECT ID FROM tbl_week_detail WHERE ID = ?';
        $res = $this->db->query($qry, array($refID));
        if($res && $res->num_rows() > 0){
            
            return true;
        }else{
            return false;
        }
    }

    function getRefDataByID($refID)
    {
        $qry = 'SELECT * FROM tbl_week_detail wd INNER JOIN tbl_subject s ON (s.id=wd.subject_id) WHERE wd.ID = ?';
        $res = $this->db->query($qry, array($refID));
        if($res && $res->num_rows() > 0){
            
            return $res->result();
        }else{
            return false;
        }
    }

}

?>