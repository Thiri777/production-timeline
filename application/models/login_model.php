<?php
class Login_model extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

    function login($username, $password)
	{

		$this->db->select('*');
		$this->db->from('tbl_users');
		$this->db->where('username', $username);
		$this->db->where('password', MD5($password));
		$this->db->limit(1);

		$query = $this->db->get();

		if($query->num_rows() == 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function checkUser($username){

		$query = $this->db->get_where('tbl_users', array('username' => $username));

		if($query->num_rows() > 0){
			return true;
		}else{return false;}
	}

	function saveNewPassword($data){
		$this->db->where(array('username' => $data['username']));
        $res = $this->db->update('tbl_users' , array('password' => $data['txtnpassword'])); 
       // echo $this->db->last_query();exit;
        if($res) return true;
        else return false;
	}
}



?>